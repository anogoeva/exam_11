const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./config');
const Category = require("./models/Category");
const Commodity = require("./models/Сommodity");
const User = require("./models/User");

const run = async () => {
  await mongoose.connect(config.db.url);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }
  const [admin, root, Aijan] = await User.create({
      username: 'admin',
      password: '123',
      name: 'super admin',
      phone: '0777777777',
      token: nanoid(),
    }, {
      username: 'root',
      password: '123',
      name: 'super root',
      phone: '0666666666',
      token: nanoid(),
    },
    {
      username: 'Aijan',
      password: '123',
      name: 'super Aijan',
      phone: '0000000000',
      token: nanoid(),
    });

  const [clothes, food, computers,] = await Category.create({
      title: 'Outerwear',
      description: 'Mink coat',
    }, {
      title: 'Chocolate',
      description: 'Dark dark chocolate with nuts',
    },
    {
      title: 'Notebook',
      description: 'Dell Model 2021',
    });

  await Commodity.create({
      title: 'Mink coat',
      price: 2000,
      description: 'sable fur coat',
      image: 'fixtures/minkcoat.jpeg',
      category: clothes,
      user: admin,
    },
    {
      title: 'Chocolate',
      price: 3,
      description: 'Dark chocolate',
      image: 'fixtures/chocolate.jpeg',
      category: food,
      user: root,
    },
    {
      title: 'Notebook',
      price: 3000,
      description: 'Laptop gaming',
      image: 'fixtures/notebook.jpeg',
      category: computers,
      user: Aijan,
    });

  await mongoose.connection.close();
};

run().catch(console.error);