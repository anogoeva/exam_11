const mongoose = require('mongoose');
const idvalidator = require('mongoose-id-validator');

const CommoditySchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  price: {
    type: Number,
    min: 0,
    required: true,
  },
  description: {
    type: String,
    required: true
  },
  image: {
    type: String,
    required: true
  },
  category: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Category',
    required: true,
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  }
});

CommoditySchema.plugin(idvalidator);
const Commodity = mongoose.model('Commodity', CommoditySchema);
module.exports = Commodity;