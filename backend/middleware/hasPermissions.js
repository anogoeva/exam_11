const Commodity = require('../models/Сommodity');

const hasPermissions = async (req, res, next) => {
  const commodity = await Commodity.findById(req.params.id);

  if (!commodity) return res.status(404).send({error: 'Commodity not found'});

  if (req.user._id.toString() !== commodity.user._id.toString()) {
    return res.status(403).send({error: 'Permissions denied'});
  }

  next();
};

module.exports = hasPermissions;