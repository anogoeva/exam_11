const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const Commodity = require('../models/Сommodity');
const auth = require("../middleware/auth");
const hasPermissions = require("../middleware/hasPermissions");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', async (req, res) => {
  try {
    const query = {};

    if (req.query.category) {
      query.category = req.query.category;
    }

    const commodities = await Commodity.find(query).populate('category', 'title');
    res.send(commodities);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.get('/:id', async (req, res) => {
  try {
    const commodity = await Commodity.findById(req.params.id).populate('category', 'title').populate('user', 'name phone');

    if (commodity) {
      res.send(commodity);
    } else {
      res.status(404).send({error: 'Commodity not found'});
    }
  } catch {
    res.sendStatus(500);
  }
});

router.post('/', auth, upload.single('image'), async (req, res) => {
  if (!req.body.title || !req.body.price || !req.body.category || !req.body.description) {
    return res.status(400).send({error: 'Data not valid'});
  }

  const commodityData = {
    title: req.body.title,
    price: req.body.price,
    category: req.body.category,
    description: req.body.description,
    user: req.user._id
  };

  if (!req.file) {
    return res.status(400).send({error: 'Data not valid'});
  }

  commodityData.image = 'uploads/' + req.file.filename;

  const commodity = new Commodity(commodityData);

  try {
    await commodity.save();
    res.send(commodity);
  } catch (e) {
    res.status(400).send({error: 'Data not valid' + e});
  }
});

router.delete('/:id', [auth, hasPermissions], async (req, res) => {
  try {
    const commodity = await Commodity.findByIdAndRemove(req.params.id);

    if (commodity) {
      res.send(`Commodity '${commodity.title} removed'`);
    } else {
      res.status(404).send({error: 'Commodity not found'});
    }
  } catch (e) {
    res.sendStatus(500);
  }
});

module.exports = router;