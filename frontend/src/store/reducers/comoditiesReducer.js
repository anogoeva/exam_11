import {
    CREATE_COMMODITY_FAILURE,
    CREATE_COMMODITY_REQUEST,
    CREATE_COMMODITY_SUCCESS,
    DELETE_COMMODITY_FAILURE,
    DELETE_COMMODITY_REQUEST,
    DELETE_COMMODITY_SUCCESS,
    FETCH_COMMODITIES_FAILURE,
    FETCH_COMMODITIES_REQUEST,
    FETCH_COMMODITIES_SUCCESS,
    FETCH_COMMODITY_FAILURE,
    FETCH_COMMODITY_REQUEST,
    FETCH_COMMODITY_SUCCESS
} from "../actions/commoditiesActions";

const initialState = {
    fetchLoading: false,
    singleLoading: false,
    commodities: [],
    commodity: null
};

const commoditiesReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_COMMODITIES_REQUEST:
            return {...state, fetchLoading: true};
        case FETCH_COMMODITIES_SUCCESS:
            return {...state, fetchLoading: false, commodities: action.payload};
        case FETCH_COMMODITIES_FAILURE:
            return {...state, fetchLoading: false};
        case FETCH_COMMODITY_REQUEST:
            return {...state, singleLoading: true};
        case FETCH_COMMODITY_SUCCESS:
            return {...state, singleLoading: false, commodity: action.payload};
        case FETCH_COMMODITY_FAILURE:
            return {...state, singleLoading: false};
        case CREATE_COMMODITY_REQUEST:
            return {...state, singleLoading: true};
        case CREATE_COMMODITY_SUCCESS:
            return {...state, singleLoading: false};
        case CREATE_COMMODITY_FAILURE:
            return {...state, singleLoading: false};
        case DELETE_COMMODITY_REQUEST:
            return {...state, singleLoading: true};
        case DELETE_COMMODITY_SUCCESS:
            return {...state, singleLoading: false};
        case DELETE_COMMODITY_FAILURE:
            return {...state, singleLoading: false};
        default:
            return state;
    }
};

export default commoditiesReducer;