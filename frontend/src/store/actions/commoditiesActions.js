import {toast} from "react-toastify";
import WarningIcon from '@material-ui/icons/Warning';
import axiosApi from "../../axiosApi";

export const FETCH_COMMODITIES_REQUEST = 'FETCH_COMMODITIES_REQUEST';
export const FETCH_COMMODITIES_SUCCESS = 'FETCH_COMMODITIES_SUCCESS';
export const FETCH_COMMODITIES_FAILURE = 'FETCH_COMMODITIES_FAILURE';

export const FETCH_COMMODITY_REQUEST = 'FETCH_COMMODITY_REQUEST';
export const FETCH_COMMODITY_SUCCESS = 'FETCH_COMMODITY_SUCCESS';
export const FETCH_COMMODITY_FAILURE = 'FETCH_COMMODITY_FAILURE';

export const CREATE_COMMODITY_REQUEST = 'CREATE_COMMODITY_REQUEST';
export const CREATE_COMMODITY_SUCCESS = 'CREATE_COMMODITY_SUCCESS';
export const CREATE_COMMODITY_FAILURE = 'CREATE_COMMODITY_FAILURE';

export const DELETE_COMMODITY_REQUEST = 'DELETE_COMMODITY_REQUEST';
export const DELETE_COMMODITY_SUCCESS = 'DELETE_COMMODITY_SUCCESS';
export const DELETE_COMMODITY_FAILURE = 'DELETE_COMMODITY_FAILURE';

export const fetchCommoditiesRequest = () => ({type: FETCH_COMMODITIES_REQUEST});
export const fetchCommoditiesSuccess = commodities => ({type: FETCH_COMMODITIES_SUCCESS, payload: commodities});
export const fetchCommoditiesFailure = () => ({type: FETCH_COMMODITIES_FAILURE});

export const fetchCommodityRequest = () => ({type: FETCH_COMMODITY_REQUEST});
export const fetchCommoditySuccess = commodity => ({type: FETCH_COMMODITY_SUCCESS, payload: commodity});
export const fetchCommodityFailure = () => ({type: FETCH_COMMODITY_FAILURE});

export const createCommodityRequest = () => ({type: CREATE_COMMODITY_REQUEST});
export const createCommoditySuccess = () => ({type: CREATE_COMMODITY_SUCCESS});
export const createCommodityFailure = () => ({type: CREATE_COMMODITY_FAILURE});

export const deleteCommodityRequest = () => ({type: DELETE_COMMODITY_REQUEST});
export const deleteCommoditySuccess = () => ({type: DELETE_COMMODITY_SUCCESS});
export const deleteCommodityFailure = () => ({type: DELETE_COMMODITY_FAILURE});


export const fetchCommodities = (query) => {
    return async (dispatch, getState) => {
        try {
            const headers = {
                'Authorization': getState().users.user && getState().users.user.token
            };
            dispatch(fetchCommoditiesRequest());
            const response = await axiosApi.get('/commodities' + query, {headers});
            dispatch(fetchCommoditiesSuccess(response.data));
        } catch (error) {
            dispatch(fetchCommoditiesFailure());
            if (error.response.status === 401) {
                toast.warning('You need login!');
            } else {
                toast.error('Could not fetch commodities!', {
                    theme: 'colored',
                    icon: <WarningIcon/>
                });
            }
        }
    };
};

export const fetchCommodity = id => {
    return async dispatch => {
        try {
            dispatch(fetchCommodityRequest());
            const response = await axiosApi.get('/commodities/' + id);
            dispatch(fetchCommoditySuccess(response.data));
        } catch (e) {
            dispatch(fetchCommodityFailure());
        }
    };
};

export const createCommodity = commodityData => {
    return async (dispatch, getState) => {
        try {
            const headers = {
                'Authorization': getState().users.user && getState().users.user.token
            };
            dispatch(createCommodityRequest());
            await axiosApi.post('/commodities', commodityData, {headers});
            dispatch(createCommoditySuccess());
            toast.success('Commodity created');
        } catch (error) {
            if (error.response.status === 400) {
                toast.error('Price must be positive number');
            }
            dispatch(createCommodityFailure());
            throw error;
        }
    };
};

export const removeCommodity = id => {
    return async (dispatch, getState) => {
        try {
            const headers = {
                'Authorization': getState().users.user && getState().users.user.token
            };
            dispatch(deleteCommodityRequest());
            await axiosApi.delete(`/commodities/${id}`, {headers});
            dispatch(deleteCommoditySuccess());
            toast.success('Successfully deleted commodity with id - ' + id);
        } catch (error) {
            if (error.response.status === 403) {
                toast.error('You don\'t have permission for this operation');
            }
            dispatch(deleteCommodityFailure(error));
        }
    };
};