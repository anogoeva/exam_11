import Layout from "./components/UI/Layout/Layout";
import {Route, Switch} from "react-router-dom";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Commodities from "./containers/Commodities/Commodities";
import NewCommodity from "./containers/NewCommodity/NewCommodity";
import Commodity from "./containers/Commodity/Commodity";

const App = () => (
    <Layout>
        <Switch>
            <Route path="/" exact component={Commodities}/>
            <Route path="/commodities/new" component={NewCommodity}/>
            <Route path="/commodities" component={Commodities}/>
            <Route path="/commodity/:id" component={Commodity}/>
            <Route path="/register" component={Register}/>
            <Route path="/login" component={Login}/>
        </Switch>
    </Layout>
);

export default App;
