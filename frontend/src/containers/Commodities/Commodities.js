import React, {useEffect} from 'react';
import {Button, CircularProgress, Grid} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {fetchCommodities} from "../../store/actions/commoditiesActions";
import CommodityItem from "../../components/CommodityItem/CommodityItem";
import {fetchCategories} from "../../store/actions/categoriesActions";
import {Link, useLocation} from "react-router-dom";

const Commodities = () => {
    const dispatch = useDispatch();
    const commodities = useSelector(state => state.commodities.commodities);
    const fetchLoading = useSelector(state => state.commodities.fetchLoading);
    const categories = useSelector(state => state.categories.categories);
    const fetchLoadingCategories = useSelector(state => state.categories.fetchLoadingCategories);
    const query = useLocation().search;

    useEffect(() => {
        dispatch(fetchCommodities(query));
    }, [dispatch, query]);

    useEffect(() => {
        dispatch(fetchCategories());
    }, [dispatch]);

    return (
        <Grid container direction="column" spacing={2}>
            <Grid item container justifyContent="space-between" alignItems="center">
                <Grid item>
                    {fetchLoadingCategories ? (
                        <Grid container justifyContent="center" alignItems="center">
                            <Grid item>
                                <CircularProgress/>
                            </Grid>
                        </Grid>
                    ) : categories.map(category => (
                        <Button component={Link} to={'/commodities?category=' + category._id} variant={"text"}
                                key={category._id}>{category.title} <br/></Button>
                    ))}

                </Grid>
            </Grid>
            <Grid item>
                <Grid item container direction="row" spacing={1}>
                    {fetchLoading ? (
                        <Grid container justifyContent="center" alignItems="center">
                            <Grid item>
                                <CircularProgress/>
                            </Grid>
                        </Grid>
                    ) : commodities.map(commodity => (
                        <CommodityItem
                            key={commodity._id}
                            id={commodity._id}
                            title={commodity.title}
                            price={commodity.price}
                            image={commodity.image}
                        />
                    ))}
                </Grid>
            </Grid>
        </Grid>
    );
};

export default Commodities;