import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchCommodity, removeCommodity} from "../../store/actions/commoditiesActions";
import {Box, Button, CardMedia, makeStyles, Paper, Typography} from "@material-ui/core";
import {apiURL} from "../../config";
import {useHistory} from "react-router-dom";

const useStyles = makeStyles({
    card: {
        height: '100%'
    },
    media: {
        height: 0,
        paddingTop: '56.25%'
    }
});
const Commodity = ({match}) => {

    const user = useSelector(state => state.users.user);
    const history = useHistory();
    const classes = useStyles();
    const dispatch = useDispatch();
    const commodity = useSelector(state => state.commodities.commodity);

    useEffect(() => {
        dispatch(fetchCommodity(match.params.id));
    }, [dispatch, match.params.id]);

    const deleteHandle = () => {
        dispatch(removeCommodity(history.location.pathname.split('/')[2]));
        history.push('/');
    };

    return commodity && (
        <Paper component={Box} p={2}>
            <Typography variant="h4">Title: <b>{commodity.title}</b></Typography>
            <Typography variant="subtitle2">Price: <i>{commodity.price} $ </i></Typography>
            <Typography variant="body1">Description: {commodity.description}</Typography>
            <Typography variant="body1">Category: {commodity.category.title}</Typography>
            <hr/>
            <Typography variant="body1">Seller: <b>{commodity.user.name}</b></Typography>
            <Typography variant="body1">Phone: <i>{commodity.user.phone}</i></Typography>
            <hr/>
            <CardMedia
                image={apiURL + '/' + commodity.image}
                title={commodity.title}
                className={classes.media}
            />
            {user ? (
                <Button variant="outlined" color="secondary" aria-controls="simple-menu" aria-haspopup="true"
                        onClick={deleteHandle}
                >Delete</Button>
            ) : ""}
        </Paper>
    );
};

export default Commodity;