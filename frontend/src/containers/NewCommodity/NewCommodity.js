import React, {useEffect} from 'react';
import {Typography} from "@material-ui/core";
import CommodityForm from "../../components/CommodityForm/CommodityForm";
import {useDispatch, useSelector} from "react-redux";
import {createCommodity} from "../../store/actions/commoditiesActions";
import {fetchCategories} from "../../store/actions/categoriesActions";

const NewCommodity = ({history}) => {
    const dispatch = useDispatch();
    const categories = useSelector(state => state.categories.categories);

    useEffect(() => {
        dispatch(fetchCategories());
    }, [dispatch]);

    const onSubmit = async commodityData => {
        await dispatch(createCommodity(commodityData));
        history.replace('/');
    };

    return (
        <>
            <Typography variant="h4">New commodity</Typography>
            <CommodityForm
                onSubmit={onSubmit}
                categories={categories}
            />
        </>
    );
};

export default NewCommodity;