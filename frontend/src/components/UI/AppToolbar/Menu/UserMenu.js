import React, {useState} from 'react';
import {Button, Grid, Menu, MenuItem} from "@material-ui/core";
import {useDispatch} from "react-redux";
import {logoutUser} from "../../../../store/actions/usersActions";
import {useHistory} from "react-router-dom";

const UserMenu = ({user}) => {
    const dispatch = useDispatch();
    const [anchorEl, setAnchorEl] = useState(null);
    const history = useHistory();

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const addHandleClick = () => {
        history.push('/commodities/new');
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <>
            <Grid container>
                <Button aria-controls="simple-menu" onClick={addHandleClick} color="inherit">
                    Add new commodity
                </Button>
                <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick} color="inherit">
                    Hello, {user.name}!
                </Button>
            </Grid>

            <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                <MenuItem onClick={() => dispatch(logoutUser())}>Logout</MenuItem>
            </Menu>
        </>
    );
};

export default UserMenu;