import React, {useState} from "react";
import {makeStyles} from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import {useSelector} from "react-redux";
import {InputLabel, MenuItem, Select} from "@material-ui/core";

const useStyles = makeStyles(theme => ({
    root: {
        marginTop: theme.spacing(2)
    },
}));

const CommodityForm = ({onSubmit}) => {

    const categories = useSelector(state => state.categories.categories);

    const classes = useStyles();
    const [state, setState] = useState({
        category: "",
        title: "",
        description: "",
        image: null,
        price: "",
    });

    const submitFormHandler = e => {
        e.preventDefault();
        const formData = new FormData();
        Object.keys(state).forEach(key => {
            formData.append(key, state[key]);
        });
        onSubmit(formData);
    };

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setState(prevState => {
            return {...prevState, [name]: file};
        });
    };

    return (
        <>
            <Grid
                container
                direction="column"
                spacing={2}
                component="form"
                className={classes.root}
                autoComplete="off"
                onSubmit={submitFormHandler}
            >
                <Grid item xs>
                    <InputLabel id="commodity-simple-select">Categories</InputLabel>
                    <Select
                        required={true}
                        fullWidth={true}
                        name="category"
                        labelId="commodity-select-label"
                        id="commodity-simple-select"
                        value={state.category}
                        label="category"
                        onChange={inputChangeHandler}
                    >
                        {categories.map(category => (
                            <MenuItem key={category._id} value={category._id}>{category.title}</MenuItem>
                        ))}
                    </Select>
                </Grid>

                <Grid item xs>
                    <TextField
                        fullWidth
                        required
                        variant="outlined"
                        label="Title"
                        name="title"
                        onChange={inputChangeHandler}
                    />
                </Grid>
                <Grid item xs>
                    <TextField
                        required={true}
                        fullWidth
                        multiline
                        rows={3}
                        variant="outlined"
                        label="Description"
                        name="description"
                        onChange={inputChangeHandler}
                    />
                </Grid>

                <Grid item xs>
                    <TextField
                        fullWidth
                        required
                        variant="outlined"
                        label="Price"
                        name="price"
                        onChange={inputChangeHandler}
                    />
                </Grid>
                <Grid item xs>
                    <TextField
                        required={true}
                        type="file"
                        name="image"
                        onChange={fileChangeHandler}
                    />
                </Grid>

                <Grid item xs>
                    <Button type="submit" color="primary" variant="contained">Create post</Button>
                </Grid>
            </Grid>
        </>
    );
};

export default CommodityForm;